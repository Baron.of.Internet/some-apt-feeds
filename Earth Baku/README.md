# An APT Group Targeting Indo-Pacific Countries With New Stealth Loaders and Backdoor

An APT group known as Earth Baku has updated its arsenal of malware tools to target companies in the Indo-Pacific region. Earth Baku, a cyberespionage and cybercriminal group, was charged by the US Department of Justice in August 2020 with computer intrusion offenses related to data theft, ransomware, and cryptocurrency mining attacks. 

more information: https://documents.trendmicro.com/assets/white_papers/wp-earth-baku-an-apt-group-targeting-indo-pacific-countries.pdf 
