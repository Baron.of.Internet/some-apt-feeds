# APT-C-61 attacks against South Asia

A series of attacks organized by an unknown APT were observed starting on early 2020. The target were important organizations such as national institutions, military industry, and scientific research in Pakistan, Bangladesh and other countries. The APT used spear phishing emails and social engineering methods to infiltrate, spread malicious programs to the target device, secretly control the target device, and continue to steal sensitive files.

more information: https://otx.alienvault.com/pulse/60f1832853d980c86f70a513
