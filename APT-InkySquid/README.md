# North Korean APT InkySquid Infects Victims Using Browser Exploits

Strategic web compromise (SWC) of the website of the Daily NK, a South Korean online newspaper that focuses on issues relating to North Korea, led to malicious code distributed on the Daily NK website, observed from at least late March 2021 until early June 2021. Researchers called the payload BLUELIGHT, and attributed the attack to the group InkySquid, which broadly corresponds to activity known publicly under the monikers ScarCruft and APT37.

more information: https://www.volexity.com/blog/2021/08/17/north-korean-apt-inkysquid-infects-victims-using-browser-exploits/
